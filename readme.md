# UserManagerDemo

This is a sample Laravel API + VueJS web application for managing User objects.

Supported operations:

- List All Users
- Show User Details (incl. addresses)
- Create User
- Edit User
- Delete User

## API Endpoints

Users:
- `GET /api/v1/users`: Returns an array of User objects
- `GET /api/v1/users/{id}`: Returns the User object specified by `{id}`
- `POST /api/v1/users`: Creates a new User object
    - Sample payload:  
        ```json
        {
            "user_roles_id": 1,
            "username": "Test",
            "email": "test@test.com"
        }
        ```
- `PUT /api/v1/users/1`: Updates the User object specified by `{id}`
    - Sample payload:
        ```json
        {
          "username": "Test-Renamed"
        }
        ```
- `DELETE /api/v1/users/1`: Deletes the User object specified by `{id}`

Addresses:
- `GET /api/v1/addresses`: Returns an array of UserAddress objects
    - `?userId={userId}` query string filters by a given User's addresses
- `GET /api/v1/addresses/{id}`: Returns the UserAddress object specified by `{id}`
- `POST /api/v1/addresses`: Creates a new UserAddress object
    - Sample payload:  
        ```json
        {
            "user_id": 1,
            "address": "222 First Street",
            "province": "Ontario",
            "city": "Ottawa",
            "country": "Canada",
            "postal_code": "K1K 2G3"
        }
        ```
- `PUT /api/v1/addresses/1`: Updates the UserAddress object specified by `{id}`
    - Sample payload:
        ```json
        {
          "address": "111 First Street"
        }
        ```
- `DELETE /api/v1/addresses/1`: Deletes the UserAddress object specified by `{id}`

Roles:
- `GET /api/v1/roles`: Returns an array of Role objects
- `GET /api/v1/roles/{id}`: Returns the Role object specified by `{id}`

## Running the demo

To test locally, you will need PHP 7.3+, composer, node/npm 5+, and a recent version of Docker installed.

- `cp .env.example .env`
- `composer install`
- `npm ci`
- `npm run production`
- `docker-compose up -d`
- Open a browser to `http://localhost:8000/`

Docker will automatically populate the test database with the supplied SQL script of data.

You can stop the demo with: `docker-compose down`  
You can remove the test database volume with: `docker volume rm usermanagerdemo_data`

## Notes

I made a few assumptions for this demo, as I found a few ambiguities in the instructions:
   
    - The database schema is immutable and shouldn't be modified
    - The demo will be run on an existing database and no migrations should be run
    - The schema is deliberately flawed (missing foreign keys, naming mismatches, null columns) in order to test my ability to correct some of those things in application layer code
    - By stating that a Restful API was required, I was to create a JS front-end that consumed that API endpoints
    
I made a few design choices for this demo:

    - While I would normally create a service/repository layer in applications, it would be way too much code for a simple demo, so the logic is in the API controllers here
    - While I would normally write tests, my time was limited and as such I did not do them for this demo
    - There is no custom error handling or "sad path" handling in this code, again, due to time restraints
    - Addresses are not editable or creatable in the JS demo, but are viewable and get deleted along with their associated user...due to time again. However the API endpoint works.
