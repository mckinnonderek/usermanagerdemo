<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserAddress extends Model
{
    const RULES = [
        'user_id'       => 'required|int|exists:users,id',
        'address'       => 'required|max:255',
        'province'      => 'required|max:255',
        'city'          => 'required|max:255',
        'country'       => 'required|max:255',
        'postal_code'   => 'required|max:255',
    ];

    protected $fillable = [
        'address', 'province', 'city', 'country', 'postal_code',
    ];

    public $timestamps = false;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
