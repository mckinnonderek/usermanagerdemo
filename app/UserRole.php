<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class UserRole extends Model
{
    protected $fillable = [
        'label'
    ];

    public $timestamps = false;

    public function users(): HasMany
    {
        return $this->hasMany(User::class, 'user_roles_id');
    }
}
