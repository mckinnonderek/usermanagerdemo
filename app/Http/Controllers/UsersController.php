<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return response()->json($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_roles_id' => 'required|integer|exists:user_roles,id',
            'username'      => 'required|max:255|unique:users',
            'email'         => 'required|max:255|unique:users',
        ]);

        $user = new User($request->all());
        $user->user_roles_id = $request->get('user_roles_id');
        $user->saveOrFail();

        return response()->json($user, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $user = User::query()->with('addresses')->findOrFail($id);

        return response()->json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'user_roles_id' => 'required|integer|exists:user_roles,id',
            'username'      => "required|max:255|unique:users,username,$id",
            'email'         => "required|max:255|unique:users,email,$id",
        ]);

        $user = User::query()->findOrFail($id);
        $user->fill($request->all());
        $user->user_roles_id = $request->get('user_roles_id');
        $user->saveOrFail();

        return response()->noContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $user = User::query()->findOrFail($id);
        $user->addresses()->delete();
        $user->delete();

        return response()->noContent();
    }
}
