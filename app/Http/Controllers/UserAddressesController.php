<?php

namespace App\Http\Controllers;

use App\UserAddress;
use Illuminate\Http\Request;

class UserAddressesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = UserAddress::query();

        if ($request->has('userId')) {
            $query->where('user_id', $request->get('userId'));
        }

        $addresses = $query->get();

        return response()->json($addresses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $request->validate(UserAddress::RULES);

        $address = new UserAddress($request->all());
        $address->user_id = $request->get('user_id');
        $address->saveOrFail();

        return response()->json($address, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $address = UserAddress::query()->findOrFail($id);

        return response()->json($address);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function update(Request $request, int $id)
    {
        $request->validate(UserAddress::RULES);

        $address = UserAddress::query()->findOrFail($id);
        $address->fill($request->all());
        $address->saveOrFail();

        return response()->noContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $address = UserAddress::query()->findOrFail($id);
        $address->delete();

        return response()->noContent();
    }
}
