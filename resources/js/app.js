import './bootstrap';
import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import App from './views/App';
import Users from './components/Users';
import ViewUser from './components/ViewUser';
import EditUser from './components/EditUser';
import CreateUser from './components/CreateUser';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'users',
            component: Users
        },
        {
            path: '/users/:id/view',
            name: 'viewUser',
            component: ViewUser,
            props: true
        },
        {
            path: '/users/:id/edit',
            name: 'editUser',
            component: EditUser,
            props: true
        },
        {
            path: '/users/create',
            name: 'createUser',
            component: CreateUser
        }
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
