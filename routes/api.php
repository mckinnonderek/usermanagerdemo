<?php

use Illuminate\Support\Facades\Route;

Route::prefix('v1')->group(function() {

    Route::apiResource('users', 'UsersController');
    Route::apiResource('addresses', 'UserAddressesController');
    Route::apiResource('roles', 'UserRolesController')->only(['index', 'show']);

});
